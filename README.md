TJTelnet v1.3.0

- A simple telnet client application which supports ANSI and CP949 (EUC-KR) encoding

* Developed & built with ADT bundle including Android SDK
* Source code is all opened.

[![88x31.png](https://bitbucket.org/repo/6geEq5/images/2180281374-88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/) All contents are under CCL(Creative Commons License).