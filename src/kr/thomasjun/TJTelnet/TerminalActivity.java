package kr.thomasjun.TJTelnet;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * 텔넷 터미널을 표시하는 Activity
 * TerminalView를 이용해 터미널을 에뮬레이션한다
 * 키보드와 특수키 입력을 처리한다
 * @author ThomasJun
 */
public class TerminalActivity extends Activity {
	public static class TelnetHandler extends Handler {
		private final WeakReference<TerminalActivity> RefTermAct;
		
		public TelnetHandler(TerminalActivity ta) {
			RefTermAct = new WeakReference<TerminalActivity>(ta);
		}
		
		@Override
		public void handleMessage(Message msg) {
			TerminalActivity termAct = RefTermAct.get();
			if (termAct != null)
				termAct.handleMessage(msg);
		}
		
		public String getResourceString(int sid) {
			return RefTermAct.get().getResources().getString(sid);
		}
	}
	
	private TelnetHandler TelHandler = null;
	
	private TerminalBuffer TermBuffer = null;
	private TerminalView TermView = null;
	
	private TelnetCommunicator TelCom = null;
	private TelnetTask TelTask = null;
	
	private boolean Ctrl = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) < Configuration.SCREENLAYOUT_SIZE_XLARGE)
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		setContentView(R.layout.terminal);

		TelHandler = new TelnetHandler(this);
		
		TermBuffer = new TerminalBuffer();
		
		TermView = new TerminalView(this, TelHandler);
		TermView.setBuffer(TermBuffer);
		TermView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));	
		((LinearLayout)findViewById(R.id.TerminalViewLayout)).addView(TermView);
		TermView.requestFocus();
		
		TelCom = new TelnetCommunicator();
		TelCom.setBuffer(TermBuffer);
		String host = getIntent().getStringExtra("Host");
		if (!TelCom.setHost(host)) {	// 호스트 문자열이 잘못된 경우
			showToast(getResources().getString(R.string.telnet_wrong).replace("[h]", host), Toast.LENGTH_SHORT);
			finish();
		}
		
		setCtrl(false);
		
		TelTask = new TelnetTask(TelCom, TelHandler);
		TelTask.execute();
	}

	/*
	 * Activity의 상태가 바뀔 때 호출되는 콜백 메소드
	 * Manifest에 configChanges에 직접 제어할 상태를 지정한다
	 * Screen orientation이 바뀔때마다 onCreate()이 호출되므로 이를 제어해준다
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	/*
	 * Activity가 끝날 때 텔넷 통신을 정리해준다
	 */
	@Override
	protected void onDestroy() {
		if (TelTask != null)
			TelTask.finishTask();
		super.onDestroy();
	}
	
	
	/*
	 * 특수키 입력을 처리한다
	 */
	public void clickSpecial(View v) {
		switch (v.getId()) {
		case R.id.CtrlImage:
			setCtrl(true);
			break;
		case R.id.CtrlOnImage:
			setCtrl(false);
			break;
		case R.id.EscImage:
			TelCom.addSendQueue(new byte[] {27});
			break;
		}
	}
	private void setCtrl(boolean c) {
		Ctrl = c;
		
		ImageView imgCtrl = (ImageView)findViewById(R.id.CtrlImage);
		ImageView imgCtrlOn = (ImageView)findViewById(R.id.CtrlOnImage);
		
		if (Ctrl) {
			imgCtrl.setVisibility(View.GONE);
			imgCtrlOn.setVisibility(View.VISIBLE);
		} else {
			imgCtrl.setVisibility(View.VISIBLE);
			imgCtrlOn.setVisibility(View.GONE);
		}
	}
	
	
	/*
	 * Handler의 메시지를 처리해준다
	 * 
	 * <Message code>
	 *  1 : Activity 종료
	 *  11 : TerminalView 업데이트
	 *  21 : 호스트로 데이터 전송
	 *  22 : 특수키를 취소하는 데이터 전송
	 *  23 : 특수키를 사용하는 데이터 전송
	 *  101 : 짧은 toast 메시지 표시
	 *  102 : 긴 toast 메시지 표시
	 *  201 : 불확실한 에러 표시
	 *  202 : 네트워크 에러 표시
	 */
	public void handleMessage(Message msg) {
		switch (msg.what) {
		case 1:
			finish();
			break;
		case 11:
			TermView.invalidate();
			break;
		case 21:
			TelCom.addSendQueue((byte[])msg.obj);
			break;
		case 22:
			setCtrl(false);
			TelCom.addSendQueue((byte[])msg.obj);
			break;
		case 23:
			byte[] b = (byte[])msg.obj;
			if (Ctrl) {
				if ((b[0] >= 0x40) && (b[0] <= 0x5F))
					b[0] -= 0x40;
				else if ((b[0] >= 0x61) && (b[0] <= 0x7A))
					b[0] -= 0x60;
			}	
			setCtrl(false);			
			TelCom.addSendQueue(b);
			break;
		case 101:
			showToast((String)msg.obj, Toast.LENGTH_SHORT);
			break;
		case 102:
			showToast((String)msg.obj, Toast.LENGTH_LONG);
			break;
		case 201:
			showErrorAlert(R.drawable.ic_dialog_error, getResources().getString(R.string.error_title), getResources().getString(R.string.error_msg));
			break;
		case 202:
			showErrorAlert(R.drawable.ic_dialog_error_network, getResources().getString(R.string.error_title), getResources().getString(R.string.error_msg_network));
			break;
		}
	}

	/*
	 * Toast 메시지를 팝업해준다
	 */
	public void showToast(String tmsg, int dur) {
		Toast.makeText(this, tmsg, dur).show();
	}
	
	/*
	 * 에러 메시지를 팝업해준다
	 */
	public void showErrorAlert(int iconid, String title, String msg) {
		AlertDialog.Builder aBuilder = new AlertDialog.Builder(this);
		aBuilder.setIcon(iconid)
			.setTitle(title)
			.setMessage(msg);
		aBuilder.setPositiveButton(R.string.error_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		
		aBuilder.create().show();
	}
}
