package kr.thomasjun.TJTelnet;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/*
 *  호스트네임 리스트를 저장하기 위한 SQLite 데이터베이스 helper 클래스 
 */
class HostListDBHelper extends SQLiteOpenHelper {
	private static final String DBName = "HostList";

	public HostListDBHelper(Context context) {
		super(context, DBName, null, 1);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE HostHistory (host TEXT, time DATETIME)");
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Cursor cur = db.rawQuery("SELECT * FROM HostHistory", null);
		int cnt = cur.getCount();
		
		String[] hosts = new String[cnt];
		String[] times = new String[cnt];
		
		int i = 0;
		cur.moveToFirst();
		while ((!cur.isAfterLast()) && (i < cnt)) {
			hosts[i] = cur.getString(0);
			times[i] = cur.getString(1);
			
			i++;			
			cur.moveToNext();
		}
		
		db.execSQL("DROP TABLE HostHistory");
		db.execSQL("CREATE TABLE HostHistory (host TEXT, time DATETIME)");
		
		for (i=0; i<cnt; i++)
			db.execSQL("INSERT INTO HostHistory VALUES ('" + hosts[i] + "', '" + times[i] + "')");
	}
}

/**
 * 어플리케이션이 실행 되었을 때 처음 실행되는 Activity
 * 이전에 접속 했던 호스트네임을 SQLite 데이터베이스를 이용해 저장하고 리스트로 보여준다
 * @author ThomasJun
 */
public class HostnameListActivity extends ListActivity {
	private EditText HostnameEdittext = null;

	private SQLiteDatabase HostnameListDB = null;
	private ArrayList<String> HostnameList = null;
	private ArrayAdapter<String> HostnameListAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.hostnamelist);
        
        
        HostnameEdittext = (EditText)findViewById(R.id.HostnameText);

        HostnameListDB = (new HostListDBHelper(this)).getWritableDatabase();
        HostnameList = new ArrayList<String>();
        HostnameListAdapter = new ArrayAdapter<String>(this, R.layout.hostnamelistitem, R.id.HostnameText, HostnameList);
        setListAdapter(HostnameListAdapter);
        
        refreshHostnameList();
        
        registerForContextMenu(getListView());
        
        
        // TEST
        // connectHost("loco.kaist.ac.kr");
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }
    

    /*
     * 리스트 항목을 클릭하면 그 텔넷 호스트로 접속
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
		connectHost(HostnameList.get(position));
    }

    
    /*
     * 리스트를 길게 눌렀을 때 뜨는 메뉴
     * 호스트에 접속하거나 리스트에서 삭제하는 메뉴를 제공
     */
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		int p = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
		menu.setHeaderTitle(HostnameList.get(p));
		menu.setHeaderIcon(R.drawable.ic_menu_terminal);
		menu.add(p, 1, Menu.NONE, getResources().getString(R.string.hostnamelist_context_connect));
		menu.add(p, 2, Menu.NONE, getResources().getString(R.string.hostnamelist_context_delete));
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int p = item.getGroupId();
		switch (item.getItemId()) {
		case 1:
			connectHost(HostnameList.get(p));
			break;
		case 2:
			delHostnameList(HostnameList.get(p));
			refreshHostnameList();
			break;
		default:
			return false;
		}
		return true;
	}
    

	/*
	 * 호스트에 접속할 때 데이터베이스에 시각과 함께 업데이트하고
	 * 삭제메뉴를 누르면 데이터베이스에서 삭제
	 */
	private void addHostnameList(String hostname) {
		HostnameListDB.execSQL("DELETE FROM HostHistory WHERE host='" + hostname + "'");
		HostnameListDB.execSQL("INSERT INTO HostHistory VALUES ('" + hostname + "', DATETIME('NOW'))");
	}
	
	private void delHostnameList(String hostname) {
		HostnameListDB.execSQL("DELETE FROM HostHistory WHERE host='" + hostname + "'");
	}
	
	private void refreshHostnameList() {
		Cursor cur = HostnameListDB.rawQuery("SELECT * FROM HostHistory ORDER BY time DESC", null);
		
		cur.moveToFirst();
		HostnameList.clear();
		while (!cur.isAfterLast()) {
			HostnameList.add(cur.getString(0));
			cur.moveToNext();
		}
		
		cur.close();

		HostnameListAdapter.notifyDataSetChanged();
	}

	
	/* 
	 * 텔넷 호스트네임을 받아 TerminalActivity에 Intent로 넘겨주어 접속한다
	 */
	public void connectHost(View v) {
		connectHost(HostnameEdittext.getText().toString());
	}
	
	private void connectHost(String host) {
		host = host.trim();
		if (host.length() == 0) {
			Toast.makeText(this, getResources().getString(R.string.hostname_empty), Toast.LENGTH_SHORT).show();
			return;
		}
		
		addHostnameList(host);
		refreshHostnameList();

		HostnameEdittext.setText(host);
		
		Intent TerminalIntent = new Intent(this, TerminalActivity.class);
		TerminalIntent.putExtra("Host", host);
		startActivity(TerminalIntent);
	}
	
}
