package kr.thomasjun.TJTelnet;


/**
 * 텔넷 통신을 통해 받은 터미널 출력 데이터를 보관하는 버퍼
 * 유니코드 입력(MS949 한글 인코딩)을 위해 작성중인 글자도 표시한다
 * @author ThomasJun
 */
public class TerminalBuffer {
	private int NumRow, NumCol;
	
	private byte[][] CharArray;
	private int[][] AttArray;
	
	private int CurRow = 0;
	private int CurCol = 0;
	private int CurAtt = 0;
	
	private int SCurRow = 0;
	private int SCurCol = 0;
	private int SCurAtt = 0;

	private byte[] Writing = {0, 0};

 	public class ATT {
		public static final int BOLD = 0x0001;
		public static final int UDLN = 0x0002;
		public static final int INVS = 0x0004;
		
		public static final int DSFT = 0;
		public static final int FSFT = 4;
		public static final int BSFT = 8;
		
		public static final int DMSK = (0x7);
		public static final int FMSK = (0x7 << FSFT);
		public static final int BMSK = (0x7 << BSFT);
	}

	public static final int SCROLL_UP = 0;
	public static final int SCROLL_DOWN = 1;
	
	
	/*
	 * 터미널 버퍼 초기화
	 */
	public TerminalBuffer() {
		this(24, 80);
	}
	
	public TerminalBuffer(int r, int c) {
		NumRow = r;
		NumCol = c;
		
		CharArray = new byte[NumRow][NumCol];
		AttArray = new int[NumRow][NumCol];
		
		clear(0);
		clearWriting();
	}
	
	
	/*
	 * 버퍼의 크기와 관련된 함수들
	 */
	public int getRowCount() {
		return NumRow;
	}
	
	public int getColCount() {
		return NumCol;
	}
	
	public void setSize(int newr, int newc) {
		byte[][] newCA = new byte[newr][newc];
		int[][] newAA = new int[newr][newc];
		
		for (int r=0; r<newr; r++) {
			for (int c=0; c<newc; c++) {
				newCA[r][c] = ' ';
				newAA[r][c] = 0;
			}
		}
		
		int minr = Math.min(NumRow, newr);
		int minc = Math.min(NumCol, newc);
		for (int r=0; r<minr; r++) {
			System.arraycopy(CharArray[NumRow-1-r], 0, newCA[newr-1-r], 0, minc);
			System.arraycopy(AttArray[NumRow-1-r], 0, newAA[newr-1-r], 0, minc);
		}
	}
	

	/*
	 * 버퍼의 데이터를 지우는 메소드
	 */
	public void clear(int dir) {
		switch(dir) {
		case 0:
			for (int r=0; r<NumRow; r++)
				clearLine(r);
			break;
		case 1:
			for (int c=CurCol; c<NumCol; c++) {
				CharArray[CurRow][c] = ' ';
				AttArray[CurRow][c] = 0;
			}
			for (int r=CurRow+1; r<NumRow; r++)
				clearLine(r);
			break;
		case 2:
			for (int r=0; r<CurRow; r++)
				clearLine(r);
			for (int c=0; c<CurCol; c++) {
				CharArray[CurRow][c] = ' ';
				AttArray[CurRow][c] = 0;
			}
			break;
		case 3:
			CurRow = Math.max(CurRow, 0);
			CurRow = Math.min(CurRow, NumRow - 1);
			for (int c=CurCol; c<NumCol; c++) {
				CharArray[CurRow][c] = ' ';
				AttArray[CurRow][c] = 0;
			}
			break;
		case 4:
			CurRow = Math.max(CurRow, 0);
			CurRow = Math.min(CurRow, NumRow - 1);
			for (int c=0; c<CurCol; c++) {
				CharArray[CurRow][c] = ' ';
				AttArray[CurRow][c] = 0;
			}
			break;
		case 5:
			CurRow = Math.max(CurRow, 0);
			CurRow = Math.min(CurRow, NumRow - 1);
			clearLine(CurRow);
			break;
		default:
		}
	}
	
	public void clearLine(int r) {
		for (int c=0; c<NumCol; c++) {
			CharArray[r][c] = ' ';
			AttArray[r][c] = 0;
		}		
	}
		

	/*
	 * 버퍼의 데이터를 읽고 쓰는 메소드
	 */
	public byte getChar(int r, int c) {
		if ((r < 0) || (r > NumRow - 1) || (c < 0) || (c > NumCol - 1))
			return 0;
		return CharArray[r][c];
	}
	
	public byte[] getCharUnicode(int r, int c) {
		byte[] retb = new byte[2];
		
		if ((r < 0) || (r > NumRow - 1) || (c < 0) || (c > NumCol - 1)) {
			retb[0] = 0;
			retb[1] = 0;
		} else 	if ((c == NumCol - 1)) {
			retb[0] = CharArray[r][c];
			retb[1] = 0;
		} else {
			retb[0] = CharArray[r][c];
			retb[1] = CharArray[r][c+1];
		}

		return retb;
	}

	public void putChar(byte c) {
		CharArray[CurRow][CurCol] = c;
		AttArray[CurRow][CurCol] = CurAtt;
		CurCol++;
		if (CurCol >= NumCol)
			nextLine();
	}

	public void putChar(char c) {
		putChar((byte) c);
	}
	
	public void putChar(byte[] b) {
		for (int i=0; i<b.length; i++)
			putChar(b[i]);
	}
	
	public int getAtt(int r, int c) {
		if ((r < 0) || (r > NumRow - 1) || (c < 0) || (c > NumCol - 1))
			return 0;
		return AttArray[r][c];
	}
		
	public void setAtt(int en) {
		switch(en) {
		case 0:
			CurAtt = 0;
			break;
		case 1:
			CurAtt |= ATT.BOLD;
			break;
		case 4:
			CurAtt |= ATT.UDLN;
			break;
		case 7:
			CurAtt |= ATT.INVS;
			break;
		case 22:
			CurAtt &= (~ATT.BOLD);
			break;
		case 24:
			CurAtt &= (~ATT.UDLN);
			break;
		case 27:
			CurAtt &= (~ATT.INVS);
			break;
		}
		if ((en >= 30) && (en <= 37))
			CurAtt = (CurAtt & (~ATT.FMSK)) | ((en - 30) << ATT.FSFT);
		else if ((en >= 40) && (en <= 47))
			CurAtt = (CurAtt & (~ATT.BMSK)) | ((en - 40) << ATT.BSFT);
	}
	
	
	/*
	 * 버퍼의 커서를 제어하는 메소드
	 */
	public int getCursorRow() {
		return CurRow;
	}
	
	public int getCursorCol() {
		return CurCol;
	}
	
	public boolean isCursor(int r, int c) {
		return ((r == CurRow) && (c == CurCol));
	}
	
	public void setCursor(int r, int c) {
		r = Math.max(r, 0);
		r = Math.min(r, NumRow - 1);
		c = Math.max(c, 0);
		c = Math.min(c, NumCol - 1);
		
		CurRow = r;
		CurCol = c;

		clearWriting();
	}

	public void saveCursor() {
		SCurRow = CurRow;
		SCurCol = CurCol;
	}
	
	public void restoreCursor() {
		CurRow = SCurRow;
		CurCol = SCurCol;
		
		clearWriting();
	}
	
	public void saveCursorAtt() {
		SCurRow = CurRow;
		SCurCol = CurCol;
		SCurAtt = CurAtt;
	}
	
	public void restoreCursorAtt() {
		CurRow = SCurRow;
		CurCol = SCurCol;
		CurAtt = SCurAtt;
		
		clearWriting();
	}
	
	public void moveCursor(int dr, int dc) {
		CurRow += dr;
		CurCol += dc;
		
		CurRow = Math.max(CurRow, 0);
		CurRow = Math.min(CurRow, NumRow - 1);
		CurCol = Math.max(CurCol, 0);
		CurCol = Math.min(CurCol, NumCol - 1);

		clearWriting();
	}

	public void cr() {
		CurCol = 0;
		
		clearWriting();
	}
	
	public void nextLine() {
		CurRow++;
		if (CurRow >= NumRow) {
			scroll(SCROLL_UP);
			CurRow = NumRow - 1;
		}
		CurCol = 0;
		
		clearWriting();
	}
	

	/*
	 * 버퍼를 스크롤 하는 메소드
	 */
	public void scroll(int dir) {
		switch (dir) {
		case SCROLL_UP:
			for (int r=0; r<NumRow-1; r++) {
				System.arraycopy(CharArray[r+1], 0, CharArray[r], 0, NumCol);
				System.arraycopy(AttArray[r+1], 0, AttArray[r], 0, NumCol);
			}
			clearLine(NumRow - 1);
			break;
		case SCROLL_DOWN:
			for (int r=NumRow-1; r>0; r--) {
				System.arraycopy(CharArray[r-1], 0, CharArray[r], 0, NumCol);
				System.arraycopy(AttArray[r-1], 0, AttArray[r], 0, NumCol);
			}
			clearLine(0);
			break;
		default:
		}
	}
	
	
	/*
	 * 입력중인 유니코드 문자 (MS949 한글 인코딩) 제어 메소드
	 */
	public boolean isWriting() {
		return (Writing[0] != 0);
	}
	
	public void clearWriting() {
		Writing[0] = 0;
		Writing[1] = 0;
	}
	
	public byte[] getWriting() {
		byte[] rb = new byte[2];
		
		rb[0] = Writing[0];
		rb[1] = Writing[1];
		
		return rb;
	}
	
	public void putWriting(String s) {
		putWriting(Encoding.toMS949KR(s));
	}
	
	public void putWriting(byte[] b) {
		if (!Encoding.isMS949KR(b))
			return;
		
		Writing[0] = b[0];
		Writing[1] = b[1];
	}
}
