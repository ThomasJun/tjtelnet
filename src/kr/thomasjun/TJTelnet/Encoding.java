package kr.thomasjun.TJTelnet;

import java.io.UnsupportedEncodingException;

/**
 * Encoding 관련 유틸
 * @author ThomasJun
 */
public class Encoding {
	public static boolean btw(byte b, int i1, int i2){
		byte b1 = (byte) i1;
		byte b2 = (byte) i2;
		return ((b >= b1) && (b <= b2));
	}

	
	/*
	 * MS949 (EUC-KR, 완성형 한글)
	 */
	public static boolean isMS949KR(char ch) {
		byte[] b = null;
		try {b = String.valueOf(ch).getBytes("MS949");} catch (UnsupportedEncodingException e) {return false;}
		return isMS949KR(b);
	}
	
	public static boolean isMS949KR(byte[] b) {
		if (b.length != 2)
			return false;
		
		if (btw(b[0], 0xA1, 0xFE) && btw(b[1], 0xA1, 0xFE))
			return true;
		if (btw(b[0], 0x81, 0xA0) && (btw(b[1], 0x41, 0x5A) || btw(b[1], 0x61, 0x7A) || btw(b[1], 0x81, 0xFE)))
			return true;
		if (btw(b[0], 0xA1, 0xC6) && (btw(b[1], 0x41, 0x5A) || btw(b[1], 0x61, 0x7A) || btw(b[1], 0x81, 0xA0)))
			return true;
		if ((b[0] == 0xC6) && btw(b[1], 0x41, 0x52))
			return true;
		return false;
	}
	
	public static String fromMS949KR(byte[] b) {
		String s;
		try {s = new String(b, "MS949");} catch (UnsupportedEncodingException e) {return "";}
		return s;
	}
	
	public static byte[] toMS949KR(String s) {
		byte[] b;
		if (s.length() != 1)
			return new byte[] {0, 0};
		try {b = s.getBytes("MS949");} catch (UnsupportedEncodingException e) {return new byte[] {0, 0};}
		return b;
	}
}
