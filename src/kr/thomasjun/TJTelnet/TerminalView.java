package kr.thomasjun.TJTelnet;

import kr.thomasjun.TJTelnet.TerminalActivity.TelnetHandler;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class TerminalView extends EditText {
	private Paint TermPaint = null;
	
	private TerminalBuffer TermBuffer = null;
	private TelnetHandler TelHandler = null;

	private float TextH = 8.0f;
	private float TextW = 4.0f;

	private static final String[] FColor = {"#E5E5E5", "#FF0000", "#00FF00", "#FFFF00",
											 "#5C5CFF", "#FF00FF", "#00FFFF", "#7F7F7F"};
	private static final String[] FColorB = {"#FFFFFF", "#FF4040", "#40FF40", "#FFFF40",
											 "#7070FF", "#FF40FF", "#40FFFF", "#909090"};
	private static final String[] BColor = {"#000000", "#CD0000", "#00CD00", "#CDCD00",
											"#0000EE", "#CD00CD", "#00CDCD", "#E5E5E5"};

	public TerminalView(Context context) {
		super(context);
	}

	public TerminalView(Context context, TelnetHandler th) {
		super(context);
		
		TelHandler = th;
		
		setBackgroundColor(Color.TRANSPARENT);
		setTextSize(1);
		
		setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		setImeOptions(EditorInfo.IME_ACTION_NONE);

		TermPaint = new Paint();
		TermPaint.setAntiAlias(true);
		TermPaint.setTypeface(Typeface.MONOSPACE);
		TermPaint.setTextSize(TextH);
		
		setLongClickable(false);
		setCursorVisible(false);
	}
	
	public void setBuffer(TerminalBuffer buf) {
		TermBuffer = buf;
	}
	
	
	/* 
	 * TerminalView의 현재 크기를 입력받아 적절한 텍스트의 크기를 계산한다
	 */
	private void resizeText(int vw, int vh) {
		if (TermBuffer == null)
				return;
		
		int bc = TermBuffer.getColCount();
		int br = TermBuffer.getRowCount();

		TextH = Math.min(((float)vw) / bc * 2.0f, ((float)vh) / br);
		TextW = TextH / 2.0f;
		
		TermPaint.setTextSize(TextH);
	}

	/* 
	 * TerminalView의 크기가 바뀌면 텍스트의 크기를 조절한다
	 */
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		resizeText(w, h);
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	/*
	 * 터치가 발생하면 소프트 키보드를 활성화 한다
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent (MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(this, 0);
		}
		
		return false;
	}
	
	/*
	 * 키 입력에 따라 반응한다
	 */
	@Override
	public boolean onKeyDown (int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_0:
		case KeyEvent.KEYCODE_1:
		case KeyEvent.KEYCODE_2:
		case KeyEvent.KEYCODE_3:
		case KeyEvent.KEYCODE_4:
		case KeyEvent.KEYCODE_5:
		case KeyEvent.KEYCODE_6:
		case KeyEvent.KEYCODE_7:
		case KeyEvent.KEYCODE_8:
		case KeyEvent.KEYCODE_9:
			synchronized (TermBuffer) {
				if (TermBuffer.isWriting())
					TelHandler.sendMessage(TelHandler.obtainMessage(12, TermBuffer.getWriting()));
				TermBuffer.clearWriting();
			}
			TelHandler.sendMessage(TelHandler.obtainMessage(21, new byte[] {(byte)(((byte)'0') + (keyCode - KeyEvent.KEYCODE_0))}));
			return true;
		case KeyEvent.KEYCODE_DPAD_UP:
		case KeyEvent.KEYCODE_DPAD_DOWN:
		case KeyEvent.KEYCODE_DPAD_LEFT:
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			synchronized (TermBuffer) {
				if (TermBuffer.isWriting())
					TelHandler.sendMessage(TelHandler.obtainMessage(12, TermBuffer.getWriting()));
				TermBuffer.clearWriting();
			}
			TelHandler.sendMessage(TelHandler.obtainMessage(21, new byte[] {27, '[', (byte)(((byte)'A') + (keyCode - KeyEvent.KEYCODE_DPAD_UP))}));
			return true;
		case KeyEvent.KEYCODE_ENTER:
			synchronized (TermBuffer) {
				if (TermBuffer.isWriting())
					TelHandler.sendMessage(TelHandler.obtainMessage(22, TermBuffer.getWriting()));
				TermBuffer.clearWriting();
			}
			TelHandler.sendMessage(TelHandler.obtainMessage(22, new byte[] {'\r'}));
			return true;
		case KeyEvent.KEYCODE_DEL:
			if (TermBuffer.isWriting())
				return false;
			TelHandler.sendMessage(TelHandler.obtainMessage(22, new byte[] {'\b'}));
			return true;
		default:
			return false;
		}
	}
	
	/*
	 * 입력 내용이 바뀌었을 때 반응한다
	 */
	@Override
	protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {		
		if (TermBuffer == null)
			return;
				
		switch (lengthAfter - lengthBefore) {
		case 0:
			synchronized (TermBuffer) {
				if (lengthBefore != 0)
					TermBuffer.putWriting(text.subSequence(start, start + lengthAfter).toString().substring(lengthAfter-1));
				else
					TermBuffer.clearWriting();
			}
			break;
		case 1:
			synchronized (TermBuffer) {
				if (TermBuffer.isWriting())
					TelHandler.sendMessage(TelHandler.obtainMessage(21, TermBuffer.getWriting()));
				TermBuffer.clearWriting();
			}
			
			byte[] b = Encoding.toMS949KR(text.subSequence(start, start + lengthAfter).toString().substring(lengthAfter-1));
			if (!Encoding.isMS949KR(b)) {
				TelHandler.sendMessage(TelHandler.obtainMessage(23, b));
			} else {
				synchronized (TermBuffer) {
					TermBuffer.putWriting(b);
				}
			}
			break;
		case -1:
			if ((lengthAfter > 0) && !TermBuffer.isWriting())
				TelHandler.sendMessage(TelHandler.obtainMessage(22, new byte[] {'\b'}));
			synchronized (TermBuffer) {
				TermBuffer.clearWriting();
			}
			break;
		default:
			synchronized (TermBuffer) {
				TermBuffer.clearWriting();
			}
		}
	}
	
	
	/*
	 * TerminalBuffer의 내용을 화면에 뿌려준다.
	 */
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {	
		super.onDraw(canvas);
		
		if (TermBuffer == null)
			return;

		int bc = TermBuffer.getColCount();
		int br = TermBuffer.getRowCount();
		
		TermPaint.setColor(Color.parseColor(BColor[0]));
		canvas.drawRect(0, 0, getWidth(), getHeight(), TermPaint);
		
		RectF FRect = new RectF(0.0f, 0.0f, TextW, TextH);
		for (int r=0; r<br; r++) {
			for (int c=0; c<bc; c++) {
				int att = TermBuffer.getAtt(r, c);
				int bci = (att & TerminalBuffer.ATT.BMSK) >> TerminalBuffer.ATT.BSFT;
			
				if (TermBuffer.isCursor(r, c) || ((att & TerminalBuffer.ATT.INVS) != 0))
					bci = 7 - bci;
				if (bci == 0)
					continue;
				TermPaint.setColor(Color.parseColor(BColor[bci]));
				
				FRect.offsetTo(c * TextW + 1.0f, r * TextH + 5.0f);
				if (TermBuffer.isCursor(r, c) && (TermBuffer.isWriting() || Encoding.isMS949KR(TermBuffer.getCharUnicode(r, c)))) {
					canvas.drawRect(FRect, TermPaint);
					FRect.offset(TextW, 0.0f);
					canvas.drawRect(FRect, TermPaint);					
					c++;
				} else {
					canvas.drawRect(FRect, TermPaint);
				}
			}
		}
		
		for (int r=0; r<br; r++) {
			for (int c=0; c<bc; c++) {
				int att = TermBuffer.getAtt(r, c);
				int fci = (att & TerminalBuffer.ATT.FMSK) >> TerminalBuffer.ATT.FSFT;
			
				if (TermBuffer.isCursor(r, c) || ((att & TerminalBuffer.ATT.INVS) != 0))
					fci = 7 - fci;
				TermPaint.setColor(Color.parseColor(((att & TerminalBuffer.ATT.BOLD) != 0) ? FColorB[fci] : FColor[fci]));

				byte[] b = (TermBuffer.isWriting() && TermBuffer.isCursor(r, c)) ? TermBuffer.getWriting() : TermBuffer.getCharUnicode(r, c);
				if (Encoding.isMS949KR(b)) {
					canvas.drawText(Encoding.fromMS949KR(b), c * TextW, (r + 1) * TextH, TermPaint);
					c++;
				} else {
					canvas.drawText(String.valueOf((char)b[0]), c * TextW, (r + 1) * TextH, TermPaint);
				}
			}
		}
	}
}
