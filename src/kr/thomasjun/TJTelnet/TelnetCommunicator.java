package kr.thomasjun.TJTelnet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.Queue;

import android.util.Log;

/**
 * 텔넷 호스트와 통신을 담당하는 클래스
 * 인터넷 소켓을 생성하고 연결을 만든 뒤
 * 텔넷 옵션 교환과 데이터 통신을 실행한다
 * @author ThomasJun
 */
public class TelnetCommunicator {
	private String TelHostName = "";
	private String TelHost = "";
	private int TelPort = 23;
	
	private Socket TelSocket = null;
	private InputStream TelInStream = null;
	private OutputStream TelOutStream = null;
	
	private TerminalBuffer TermBuffer = null;
	
	private Queue<byte[]> SendDataQueue = new LinkedList<byte[]>();
	
	class CMD {
		public static final int EOF		= 236;
		public static final int EOR		= 239;
		public static final int SE		= 240;
		public static final int SB		= 250;
		public static final int WILL	= 251;
		public static final int WONT	= 252;
		public static final int DO		= 253;
		public static final int DONT	= 254;
		public static final int IAC		= 255;
    }
	class OPT {
		public static final int ECHO		= 1;
		public static final int SGA			= 3;
		public static final int STATUS 		= 5;
		public static final int LOGOUT 		= 18;
		public static final int TTYPE		= 24;
		public static final int EOR			= 25;
		public static final int NAWS		= 31;
		public static final int TSPEED		= 32;
		public static final int LFLOW		= 33;
		public static final int XDISPLOC	= 35;
		public static final int NEWENV		= 39;
	}
	
	
	/*
	 * 전역변수 Setter/Getter
	 */
	public boolean setHost(String host) {
		TelHostName = host;
		String[] hostsplit = host.split(":");
		if ((hostsplit.length > 2) || (hostsplit.length < 1))
			return false;
		
		return setHost(hostsplit[0].trim(), (hostsplit.length == 2) ? Integer.parseInt(hostsplit[1]) : 23);		
	}
	
	public boolean setHost(String host, int port) {
		if ((host.trim().length() == 0) || (port == 0))
			return false;
		
		TelHost = host;
		TelPort = port;
		
		return true;
	}
	
	public String getHost() {
		return TelHost;
	}
	
	public int getPort() {
		return TelPort;		
	}
	
	public String getHostName() {
		return TelHostName;
	}
	
	public void setBuffer(TerminalBuffer buf) {
		TermBuffer = buf;
	}

	public boolean isConnected() {
		return (TelSocket == null) ? false : TelSocket.isConnected();
	}
	
	
	/*
	 * 텔넷 호스트에 접속한다
	 * Return code로 결과를 리턴한다.
	 * 예외가 발생할 경우 error code를 리턴한다.
	 * 
	 * <Return code>
	 *  1 : 접속 성공
	 *  11 : 잘못된 호스트
	 * <Error code>
	 *  101 : 접속 네트워크 에러
	 *  102 : 접속 중 불명확한 에러
	 */
	public int connect() {
		if (isConnected())
			return 1;
		if ((TelHost.trim().length() == 0) || (TelPort == 0))
			return 11;
		
		try {
			if (TelSocket != null)
				TelSocket.close();
			
			TelSocket = new Socket();
			TelSocket.connect(new InetSocketAddress(TelHost, TelPort), 3000);
			TelInStream = TelSocket.getInputStream();
			TelOutStream = TelSocket.getOutputStream();
			TelSocket.setSoTimeout(300);
		} catch (IOException e) {
			Log.e("TJTelnet", "TelnetCommunicator.connect()", e);
			return 101;
		} catch (Exception e) {
			Log.e("TJTelnet", "TelnetCommunicator.connect()", e);
			return 102;
		}
		
		return 1;
	}
	
	/*
	 * 텔넷 옵션을 교환한다
	 */
	public void negotiate() {
		if (!isConnected())
			return;

		try {
			sendOpt(CMD.DO, OPT.SGA);
			sendOpt(CMD.WILL, OPT.SGA);
			sendOpt(CMD.WILL, OPT.NAWS);
		} catch (Exception e) {
			Log.e("TJTelnet", "TelnetCommunicator.connect()", e);
			return;
		}
	}
	
	/*
	 * 로그아웃 및 접속 종료하고 연결을 정리한다
	 */
	public void logout() {
		try {
			sendOpt(CMD.DO, OPT.LOGOUT);
		} catch (Exception e) {
			Log.e("TJTelnet", "TelnetCommunicator.logout()", e);
		}
	}
	
	public void disconnect() {
		try {
			if (TelSocket != null) {
				TermBuffer = null;
				
				TelSocket.shutdownInput();
				TelSocket.shutdownOutput();
				TelInStream = null;
				TelOutStream = null;
				
				TelSocket.close();
				TelSocket = null;
			}
		} catch (Exception e) {}
	}
	
	
	/*
	 * 호스트로 데이터를 전송한다
	 * 전송할 데이터는 큐에 저장되어서 FIFO 방식으로 저장되고 꺼내어진다
	 * Byte 단위의 데이터를 OutputStream을 이용해 전송한다
	 */
	public void sendData() throws IOException {
		if (SendDataQueue.isEmpty())
			return;
		
		TelOutStream.write(SendDataQueue.poll());
		TelOutStream.flush();
	}
	
	public void addSendQueue(byte[] data) {
		SendDataQueue.add(data);
	}
	
	public void addSendQueue(int... data) {
		byte[] bData = new byte[data.length];
		for (int i=0; i<data.length; i++)
			bData[i] = (byte)data[i];
		
		SendDataQueue.add(bData);
	}


	/*
	 * 호스트로 텔넷 옵션을 전송한다
	 */
	public void sendOpt(int cmd, int opt) {
		addSendQueue(CMD.IAC, cmd, opt);	
	}

	public void sendSubOpt(int opt, int... subopt) {
		int bl = subopt.length + 5, optcnt = subopt.length;
		int[] data = new int[bl];
		
		data[0] = CMD.IAC;
		data[1] = CMD.SB;
		data[2] = opt;
		for (int i=0; i<optcnt; i++)
			data[i+3] = subopt[i];
		data[bl-2] = CMD.IAC;
		data[bl-1] = CMD.SE;
		addSendQueue(data);
	}
	
	
	/*
	 * 호스트로부터 옵션,서브 옵션,escape 문자를 받았을 때 필요한 반응을 한다
	 */
	public void recieveOpt() throws IOException {
		int cmd = TelInStream.read();
		int opt = TelInStream.read();
		
		switch (cmd) {
		case CMD.DO:
			switch (opt) {
			case OPT.ECHO:	// 받아들이는 옵션
			case OPT.SGA:
			case OPT.NAWS:
			case OPT.TTYPE:
			case OPT.TSPEED:
			case OPT.NEWENV:
				sendOpt(CMD.WILL, opt);
				break;
			case OPT.XDISPLOC:	// 받아들이지 않는 옵션
			case OPT.LFLOW:
				sendOpt(CMD.WONT, opt);
				break;
			default:
			}
			break;
		case CMD.WILL:
			switch (opt) {
			case OPT.ECHO:	// 받아들이는 옵션
			case OPT.SGA:
			case OPT.EOR:
				sendOpt(CMD.DO, opt);
				break;
			case OPT.STATUS:	// 받아들이지 않는 옵션
				sendOpt(CMD.DONT, opt);
			default:
			}
			break;
		case CMD.DONT:
			sendOpt(CMD.WONT, opt);
			break;
		case CMD.WONT:
			sendOpt(CMD.DONT, opt);
			break;
		case CMD.SB:
			recieveSubOpt(opt);
			break;
		default:
		}
	}

	public void recieveSubOpt(int opt) throws IOException {
		byte[] value = new byte[256];
		int r1, r2, i;
		
		r1 = TelInStream.read();
		i = 0;
		while (true) {
			r2 = TelInStream.read();
			if ((r1 == CMD.IAC) && (r2 == CMD.SE))
				break;
			value[i++] = (byte) r1;
			r1 = r2;
		}
		
		switch (opt) {
		case OPT.TSPEED:	// 필요한 정보를 전송해주는 서브 옵션
			sendSubOpt(OPT.TSPEED, 0, '3', '8', '4', '0', '0', ',', '3', '8', '4', '0', '0');
			break;
		case OPT.NEWENV:
			sendSubOpt(OPT.NEWENV, 0);
			break;
		case OPT.TTYPE:
			sendSubOpt(OPT.TTYPE, 0, 'X', 'T', 'E', 'R', 'M');
			break;
		default:
		}
	}

	public void recieveEscSeq() throws IOException {
		int[] value = {0, 0, 0};
		int r, k;
		boolean l;
				
		r = TelInStream.read();

		switch (r) {
		case '(':
		case ')':
		case '#':
			r = TelInStream.read();
			return;
		case 'M':
			TermBuffer.scroll(TerminalBuffer.SCROLL_DOWN);
			break;
		case '[':
			break;
		default:
			return;
		}
		
		k = 0;
		l = true;
		while (l) {
			l = false;
			
			r = TelInStream.read();			
			switch (r) {
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9': 
				value[k] = value[k] * 10 + (r - 0x30);
				l = true;
				break;
			case ';':
				k++;
				l = true;
				break;
			case 'A':
				TermBuffer.moveCursor(0, -Math.max(value[0], 1));
				break;
			case 'B':
				TermBuffer.moveCursor(0, Math.max(value[0], 1));
				break;
			case 'C':
				TermBuffer.moveCursor(Math.max(value[0], 1), 0);
				break;
			case 'D':
				TermBuffer.moveCursor(-Math.max(value[0], 1), 0);
				break;
			case 'S': case 's':
				TermBuffer.saveCursorAtt();
				break;
			case 'U': case 'u':
				TermBuffer.restoreCursorAtt();
				break;
			case 'H': case 'f':
				TermBuffer.setCursor(value[0] - 1, value[1] - 1);
				break;
			case 'm':
				for (int i=0; i<=k; i++)
					TermBuffer.setAtt(value[i]);
				break;
			case 'J': case 'j':
				switch (value[0]) {
				case 0:
					TermBuffer.clear(1);
					break;
				case 1:
					TermBuffer.clear(2);
					break;
				case 2:
					TermBuffer.clear(0);
					break;
				}
				break;
			case 'K': case 'k':
				switch (value[0]) {
				case 0:
					TermBuffer.clear(3);
					break;
				case 1:
					TermBuffer.clear(4);
					break;
				case 2:
					TermBuffer.clear(5);
					break;
				}
				break;
			case 'F':
				break;
			case 'G':
				break;
			case 'L':
				break;
			case 'M':
				break;
			case '@':
				break;
			case 'P':
				break;
			case 'O':
				break;
			case 'r':
				break;
			}
		}
	}
	
	
	/*
	 * 호스트로부터 데이터를 수신하는 프로세스
	 * 접속이 끝날 때 까지 메인루프에서 반복해야한다
	 * InputStream으로 부터 데이터를 받는 timeout은 300ms
	 * 데이터를 처리한 뒤 필요한 작업을 위해 return code를 반환한다
	 * 예외가 발생할 경우 error code를 반환한다
	 *  
	 * <Return code>
	 *  1 : 프로세스를 계속 진행한다
	 *  2 : 터미널을 업데이트 하고 프로세스를 계속 진행한다
	 *  11 : 호스트에서 접속 종료 신호  
	 * <Error code>
	 *  101 : 네트워크 에러 발생
	 *  102 : 불명확한 예외 발생
	 */
	public int receiveProcess() {
		try {			
			int r = TelInStream.read();
			
			synchronized (TermBuffer) {
				if (r < 0)
					return 11;
				
				switch (r) {
				case CMD.IAC:
					recieveOpt();
					break;
				case 27:
					recieveEscSeq();
					break;
				case 13:
					TermBuffer.cr();
					r = TelInStream.read();
					if (r == 10)
						TermBuffer.nextLine();
					break;
				case 10:
					TermBuffer.nextLine();
				case 7:
					break;
				case 2:
					TermBuffer.putChar(' ');
					break;
				case 8:
					TermBuffer.moveCursor(0, -1);
					break;
				default:
					TermBuffer.putChar((byte)r);
				}
			}
		} catch (SocketTimeoutException e) {
			return 2;
		} catch (IOException e) {
			Log.e("TJTelnet", "TelnetCommunicator.receiveProcess()", e);
			return 101;
		} catch (Exception e) {
			Log.e("TJTelnet", "TelnetCommunicator.receiveProcess()", e);
			return 102;
		}
		
		return 1;
	}
	

	/*
	 * 호스트로 데이터를 송신하는 프로세스
	 * 접속이 끝날 때 까지 메인루프에서 반복해야한다
	 * 데이터를 처리한 뒤 필요한 작업을 위해 return code를 반환한다
	 * 예외가 발생할 경우 error code를 반환한다
	 *  
	 * <Return code>
	 *  1 : 프로세스를 계속 진행한다
	 * <Error code>
	 *  101 : 네트워크 에러 발생
	 *  102 : 불명확한 예외 발생
	 */
	public int sendProcess() {		
		try {
			if (!SendDataQueue.isEmpty())
				sendData();
		} catch (IOException e) {
			Log.e("TJTelnet", "TelnetCommunicator.sendProcess()", e);
			return 101;
		} catch (Exception e) {
			Log.e("TJTelnet", "TelnetCommunicator.sendProcess()", e);
			return 102;
		}
		
		return 1;
	}
}