package kr.thomasjun.TJTelnet;

import java.util.concurrent.atomic.AtomicBoolean;

import kr.thomasjun.TJTelnet.TerminalActivity.TelnetHandler;
import android.os.AsyncTask;

/**
 * 텔넷 통신을 비동기 스레드에서 진행하는 AsyncTask 클래스
 * TelnetCommunicator 인스턴스를 이용해 텔넷 호스트와 통신하고
 * TerminalActivity 인스턴스에서 TerminalView를 업데이트 하도록 한다
 * @author ThomasJun
 */
public class TelnetTask extends AsyncTask<Void, Void, Integer> {
	class TelnetThread extends Thread {
		protected AtomicBoolean Running = new AtomicBoolean(false);
		protected AtomicBoolean Loop = new AtomicBoolean(false);
		
		public boolean isLoopOn() {
			return Loop.get();
		}
		
		public boolean isRunning() {
			return Running.get();
		}
		
		public void startRunning() {
			Running.set(true);
		}
		
		public void stopRunning() {
			Running.set(false);
		}
		
		public void startLoop() {
			Loop.set(true);
		}
		
		public void stopLoop() {
			Loop.set(false);
		}
	}
	
	private TelnetCommunicator TelCom = null;
	private TelnetHandler TelHandler = null; 
	
	private TelnetThread TelReceiveThread = new TelnetThread() {
		@Override
		public void run() {
			if(!TelCom.isConnected())
				return;
			startRunning();
			
			int pre = 0, cur;
			while (isRunning()) {
				startLoop();
				cur = TelCom.receiveProcess();
				switch (cur) {
				case 1:
					break;
				case 2:
					if (pre != cur)
						TelHandler.sendEmptyMessage(11);
					break;
				case 11:
					stopAllRunning();
					TelHandler.sendEmptyMessage(1);
					break;
				case 101:
					TelHandler.sendEmptyMessage(202);
					break;
				case 102:
					TelHandler.sendEmptyMessage(201);
					break;
				}
				pre = cur;
				stopLoop();
			}
		}
	};
	private TelnetThread TelSendThread = new TelnetThread() {
		@Override
		public void run() {
			if(!TelCom.isConnected())
				return;
			startRunning();
			
			while (isRunning()) {
				startLoop();
				switch (TelCom.sendProcess()) {
				case 1:
					break;
				case 101:
					TelHandler.sendEmptyMessage(202);
					break;
				case 102:
					TelHandler.sendEmptyMessage(201);
					break;
				}
				stopLoop();
			}
		}
	};
	
	/*
	 * 생성자를 이용해 연결된 인스턴스를 설정
	 */
	public TelnetTask(TelnetCommunicator tc, TelnetHandler th) {
		TelCom = tc;
		TelHandler = th;
	}

	/*
	 * TelnetCommunicator를 이용해 텔넷 호스트에 접속한다
	 */
	@Override
	protected Integer doInBackground(Void... params) {
		int ret = TelCom.connect();
		
		if (ret != 0) {
			TelReceiveThread.start();
			TelSendThread.start();
	
			TelCom.negotiate();
		}

		return ret;
	}
	
	/*
	 * 텔넷 호스트 접속시도 후 결과에 대해 UI 업데이트 
	 */
	@Override
	protected void onPostExecute(Integer result) {
		switch (result) {
		case 1:
			TelHandler.sendMessage(TelHandler.obtainMessage(101, TelHandler.getResourceString(R.string.telnet_connect).replace("[h]", TelCom.getHostName())));
			break;
		case 11:
			TelHandler.sendMessage(TelHandler.obtainMessage(102, TelHandler.getResourceString(R.string.telnet_wrong).replace("[h]", TelCom.getHostName())));
			TelHandler.sendEmptyMessage(1);
			break;
		case 101:
			TelHandler.sendEmptyMessage(202);
			break;
		case 102:
			TelHandler.sendEmptyMessage(201);
			break;
		}
	}
	
	public void stopAllRunning() {
		TelReceiveThread.stopRunning();
		TelSendThread.stopRunning();
	}
	
	/*
	 * 텔넷 호스트와의 접속 종료
	 */
	public void finishTask() {
		if (!TelCom.isConnected())
			return;
		
		TelCom.logout();

		if (TelReceiveThread.isRunning())
			TelReceiveThread.stopRunning();
		if (TelSendThread.isRunning())
			TelSendThread.stopRunning();
		while (TelReceiveThread.isLoopOn() || TelSendThread.isLoopOn());
	
		TelCom.disconnect();

		TelHandler.sendMessage(TelHandler.obtainMessage(101,  TelHandler.getResourceString(R.string.telnet_disconnect).replace("[h]", TelCom.getHostName())));
	}
}
